"""
Created on Aug 7, 2012

@author: Christophe Burki
@author_email: christophe.burki@gmail.com
"""

# ------------------------------------------------------------------------------

from distutils.core import setup

# ------------------------------------------------------------------------------

def version():
    #
    # Read the version from VERSION file
    #

    f = open('VERSION', 'r')
    v = f.readline().strip()
    f.close()

    return v

# ------------------------------------------------------------------------------

setup(name='pylogger',
      version=version(),
      description='Configurable logger for python',
      author='Christophe Burki',
      author_email='christophe.burki@gmail.com',
      url='http://gitlab.burkionline.net:8443/cburki/pylogger',
      packages=['pylogger'],
      data_files=[('./', ['VERSION'])])

# ------------------------------------------------------------------------------
