# 
# Filename     : FileWriter.py
# Description  : Class allowing to write log into a file.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

from pylogger.writer import Writer
from pylogger.formatter import Formatter

# ------------------------------------------------------------------------------

class FileWriter(Writer):
    """
    This class allow to log messages to a speficied file.
    """

    # --------------------------------------

    def __init__(self, filepath, append):
        """
        Constructor

        :param filepath: The path of the file where to write the messages
        :type filepath: string
        :param append: Whether to append to the file or truncate it
        :type append: boolean
        """

        Writer.__init__(self)

        mode = "w"
        if append:
            mode = "a"

        self.__logFile = open(filepath, mode)

    # --------------------------------------

    def __del__(self):
        """
        Destructor
        """

        self.__logFile.close()

    # --------------------------------------

    def write(self, msg, name, level, date, location):
        """
        Write a message to a file.

        :param msg: The message to write
        :type msg: string
        :param name: The name of the logger
        :type name: string
        :param level: The log level
        :type level: LoggerLevel object
        :param date: The time and date when the message is written
        :type date: Arrow object
        :param location: Location from where the message is written
        :type location: Location object
        """

        formatter = Formatter(self._logFormat, self._datetimeFormat)
        out = formatter.formatLog(msg, name, level, date, location)
        self.__logFile.write(out + '\n')
        self.__logFile.flush()

# ------------------------------------------------------------------------------
