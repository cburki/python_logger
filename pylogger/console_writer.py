# 
# Filename     : ConsoleWriter.py
# Description  : Class allowing to write message on the console (sys.stdout).
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

import sys

from pylogger.writer import Writer
from pylogger.formatter import Formatter

# ------------------------------------------------------------------------------

class ConsoleWriter(Writer):
    """
    This class allow to log messages to the console (sys.stdout).
    """

    # --------------------------------------

    def __init__(self):
        """
        Constructor
        """

        Writer.__init__(self)

    # --------------------------------------

    def write(self, msg, name, level, date, location):
        """
        Write a message to the console.

        :param msg: The message to write
        :type msg: string
        :param name: The name of the logger
        :type name: string
        :param level: The log level
        :type level: LoggerLevel object
        :param date: The time and date when the message is written
        :type date: Arrow object
        :param location: Location from where the message is written
        :type location: Location object
        """

        formatter = Formatter(self._logFormat, self._datetimeFormat)
        out = formatter.formatLog(msg, name, level, date, location)
        sys.stdout.write(out + "\n")

# ------------------------------------------------------------------------------
