# 
# Filename     : Logger.py
# Description  : Class that write the log for the different levels.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

from inspect import stack, getframeinfo
from arrow import now

from pylogger.logger_level import LoggerLevel
from pylogger.location import Location
from pylogger.multi_writer import MultiWriter

# ------------------------------------------------------------------------------

class Logger():
    """
    Class representing a logger. A logger is an entity that is used to log
    messages with a specific level to a given writer.
    """

    # --------------------------------------

    def __init__(self, level, name, writer):
        """
        Constructor

        :param level: The level of the log
        :type level: LoggerLevel object
        :param name: The name of the logger
        :type name: string
        :param writer: The writer to use to write the log
        :type writer: Writer object
        """

        self.__level = level
        self.__name = name
        self.__writer = writer

    # --------------------------------------

    def write(self, level, location, msg):
        """
        Write the message.

        :param level: The level of the message
        :type level: LoggerLevel object
        :param location: The location of the log
        :type location: Location object
        :param msg: The message to write
        :type msg: string
        """

        self.__writer.write(msg, self.__name, level, now(), location)

    # --------------------------------------

    def log(self, level, msg):
        """
        Log a message according to the given level.

        :param level: The level of the message
        :type level: LoggerLevel object
        :param msg: The message to log
        :type msg: string
        """

        caller = stack()[2]
        frame = caller[0]
        info = getframeinfo(frame)
        location = Location(info.filename, info.function, info.lineno)

        if self.__level <= level:

            self.write(level, location, msg)

    # --------------------------------------

    def emerg(self, msg):
        """
        Log an emergency message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.EMERG, msg)

    # --------------------------------------

    def alert(self, msg):
        """
        Log an alert message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.ALERT, msg)

    # --------------------------------------

    def crit(self, msg):
        """
        Log a crtitcal message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.CRIT, msg)

    # --------------------------------------

    def err(self, msg):
        """
        Log an error message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.ERR, msg)

    # --------------------------------------

    def warning(self, msg):
        """
        Log a warning message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.WARNING, msg)

    # --------------------------------------

    def notice(self, msg):
        """
        Log a notice message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.NOTICE, msg)

    # --------------------------------------

    def info(self, msg):
        """
        Log an info message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.INFO, msg)

    # --------------------------------------

    def debug(self, msg):
        """
        Log a debug message.

        :param msg: The message to log
        :type msg: string
        """

        self.log(LoggerLevel.DEBUG, msg)

    # --------------------------------------

    def setLevel(self, level):
        """
        Set the log level.

        :param level: The log level to set
        :type level: LoggerLevel object
        """

        self.__level = level

    # --------------------------------------

    def getLevel(self):
        """
        Return the log level.
        """

        return self.__level

    # --------------------------------------

    def setWriter(self, writer):
        """
        Set the writer.

        :param writer: The writer to set
        :type writer: Writer object
        """

        self.__writer = writer

    # --------------------------------------

    def addWriter(self, name, writer):
        """
        Add a writer to a multi writer. It does not add anything if
        the writer is not a multi writer.

        :param name: The name of the writer to add
        :type name: string
        :param writer: The writer to set
        :type writer: Writer object
        """

        if not isinstance(self.__writer, MultiWriter):

            print("ERROR : Could not add a writer to a non multi writer")
            return

        if name in self.__writer.keys():

            print('WARNING : Writer with that name "{}" already exist, override it'.format(name))

        self.__writer[name] = writer

    # --------------------------------------

    def getWriter(self, name=None):
        """
        Return the writer. Return the writer from a multi writer when name
        is given.

        :param name: The name of the writer to return
        :type name: string
        """

        if name is None:
            return self.__writer

        if not isinstance(self.__writer, MultiWriter):

            print("ERROR : Could not get a writer with name from a non multi writer")
            return None

        if name not in self.__writer.keys():

            print('WARNING : No writer exist with the name "{}"'.format(name))
            return None

        return self.__writer[name]

    # --------------------------------------

    def removeWriter(self, name):
        """
        Remove a writer from a multi writer. It does not remove anything if the
        writer is not a multi writer.

        :param name: The name of the writer to remove
        :type name: string
        """

        if not isinstance(self.__writer, MultiWriter):

            print("ERROR : Could not remove a writer with name from a non multi writer")
            return

        del self.__writer[name]

# ------------------------------------------------------------------------------
