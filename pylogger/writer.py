# 
# Filename     : writer.py
# Description  : Logger writer abstract class.
# Maintainer   : Christophe Burki
# 
#####################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
# 
#####################################################################

# ------------------------------------------------------------------------------

from abc import ABCMeta, abstractmethod

from pylogger.formatter import DEFAULT_LOG_FORMAT, DEFAULT_DATETIME_FORMAT

# ------------------------------------------------------------------------------

class Writer():
    """
    Abstract class representing a log writer.
    """

    __metaclass__ = ABCMeta

    # --------------------------------------

    def __init__(self):
        """
        Constructor
        """

        self._logFormat = DEFAULT_LOG_FORMAT
        self._datetimeFormat = DEFAULT_DATETIME_FORMAT

    # --------------------------------------

    @abstractmethod
    def write(self, msg, name, level, date, location):
        """
        Write a message.

        :param msg: The message to write
        :type msg: string
        :param name: The name of the logger
        :type name: string
        :param level: The log level
        :type level: LoggerLevel object
        :param date: The time and date when the message is written
        :type date: Arrow object
        :param location: Location from where the message is written
        :type location: Location object
        """

        raise NotImplementedError

    # --------------------------------------

    def setLogFormat(self, logFormat):
        """
        Set the log format for the writer.

        :param logFormat: The format to set
        :type logFormat: string
        """

        self._logFormat = logFormat

    # --------------------------------------

    def setDatetimeFormat(self, datetimeFormat):
        """
        Set the date and time format for the writer.

        :param datetimeFormat: The format to set
        :type datetimeFormat: string
        """

        self._datetimeFormat = datetimeFormat

# ------------------------------------------------------------------------------
