# 
# Filename     : location.py
# Description  : Handle log message location.
# Author       : Christophe Burki
# 
#####################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
# 
#####################################################################

# ------------------------------------------------------------------------------

class Location:
    """
    Class representing the location from where the message is written. This is
    the file name, the function  name and line number where the call to the log
    function is made.
    """

    # --------------------------------------

    def __init__(self, filename, funcname, line):
        """
        Constructor

        :param filename: The file name from which the log is written
        :type filename: string
        :param funcname: The function name from which the log is written
        :type funcname: string
        :pram line: The line numbner where the log is written
        :type line: int
        """

        self.__filename = filename
        self.__funcname = funcname
        self.__line = line

    # --------------------------------------

    def getFilename(self):
        """
        Return the file name.
        """

        return self.__filename

    # --------------------------------------

    def getFuncname(self):
        """
        Return the function name.
        """

        return self.__funcname

    # --------------------------------------

    def getLine(self):
        """
        Return the line numnber.
        """

        return self.__line

# ------------------------------------------------------------------------------
