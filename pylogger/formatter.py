# 
# Filename     : Formatter.py
# Description  : Class allowing to format the output.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

import arrow

from pylogger.logger_level import LoggerLevel

# ------------------------------------------------------------------------------

DEFAULT_LOG_FORMAT = "%d %l [%n] : %m"
DEFAULT_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# ------------------------------------------------------------------------------

class Formatter():
    """
    """

    # --------------------------------------

    def __init__(self, logFormat, datetimeFormat):
        """
        """

        self.__logFormat = logFormat
        self.__datetimeFormat = datetimeFormat

    # --------------------------------------

    def formatLog(self, msg, name, level, date, location):
        """
        """

        message = self.__logFormat
        replacement = {'%d': self.formatDatetime(date),
                       '%l': LoggerLevel.logLevel2String[level],
                       '%n': name,
                       '%m': msg,
                       '%f': location.getFilename(),
                       '%c': location.getFuncname(),
                       '%b': str(location.getLine())}

        for key, value in replacement.items():

            message = message.replace(key, value)

        return message

    # --------------------------------------

    def formatDatetime(self, date):
        """
        """

        datetimeFormat = self.__datetimeFormat
        replacement = {'%Y': 'YYYY',  # Year (4 digits)
                       '%m': 'MM',    # Month as a decimal number (2 digits)
                       '%d': 'DD',    # Day of the month as a decimal number (2 digits)
                       '%H': 'HH',    # Hour in 24 h format
                       '%M': 'mm',    # Minutes
                       '%S': 'ss'}    # Sedonds
        for key, value in replacement.items():

            datetimeFormat = datetimeFormat.replace(key, value)

        return arrow.get(date).format(datetimeFormat)

# ------------------------------------------------------------------------------
