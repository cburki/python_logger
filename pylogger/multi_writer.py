# 
# Filename     : multi_writer.py
# Description  : Writer for writing message to several writers simultaneously.
# Maintainer   : Christophe Burki
# 
#####################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
# 
#####################################################################

# ------------------------------------------------------------------------------

from pylogger.writer import Writer

# ------------------------------------------------------------------------------

class MultiWriter(Writer):
    """
    This class allow to writer mesages to several writers simultaneously.
    """

    # --------------------------------------

    def __init__(self, writers):
        """
        Constructor

        :param writers: The list of writers where to write messages
        :type writers: dictionary of Writer objects
        """

        Writer.__init__(self)
        self.__writers = writers

    # --------------------------------------

    def write(self, msg, name, level, date, location):
        """
        Write a message to several writers.

        :param msg: The message to write
        :type msg: string
        :param name: The name of the logger
        :type name: string
        :param level: The log level
        :type level: LoggerLevel object
        :param date: The time and date when the message is written
        :type date: Arrow object
        :param location: Location from where the message is written
        :type location: Location object
        """

        for writer in self.__writers.values():

            writer.write(msg, name, level, date, location)

    # --------------------------------------

    def keys(self):
        """
        Return the keys of the writers dictionary. It return a list
        of writer names.
        """

        return self.__writers.keys()

    # --------------------------------------

    def values(self):
        """
        Return the values of the writers dictionary. It return a list
        of writers.
        """

        return self.__writers.values()

    # --------------------------------------

    def items(self):
        """
        Return the items of the writers dictionary. It return a list of
        tuple with name and writer.
        """

        return self.__writers.items()

    # --------------------------------------

    def __getitem__(self, key):
        """
        Return the writer with the given key (name).

        :param key: The name of the writer to return
        :type key: string
        """

        return self.__writers[key]

    # --------------------------------------

    def __setitem__(self, key, value):
        """
        Set a writer (value) with the given key (name).

        :param key: The name of the writer to set
        :type key: string
        :param value: The writer to add
        :type value: Writer objet
        """

        self.__writers[key] = value

    # --------------------------------------

    def __delitem__(self, key):
        """
        Remove the writer with the given key (name).

        :param key: The name of the writer to remove
        :type key: string
        """

        del self.__writers[key]

# ------------------------------------------------------------------------------
