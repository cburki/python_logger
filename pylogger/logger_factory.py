# 
# Filename     : LoggerFactory.py
# Description  : Static class that allow to create a logger.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

from pylogger.property_configurator import PropertyConfigurator
from pylogger.logger import Logger
from pylogger.formatter import DEFAULT_LOG_FORMAT, DEFAULT_DATETIME_FORMAT
from pylogger.console_writer import ConsoleWriter
from pylogger.file_writer import FileWriter
from pylogger.multi_writer import MultiWriter

# ------------------------------------------------------------------------------

class LoggerFactory():
    """
    Static class for creating logger based on logging settings.
    """

    # --------------------------------------

    __loggers = {}

    # --------------------------------------

    def __init__(self):
        """
        Constructor
        """

        pass

    # --------------------------------------

    @staticmethod
    def getLogger(name):
        """
        Return a logger. This could be a new instance or an existing one if it
        was already instanciated.

        :param name: The name of the logger
        :type name: string
        """

        # Check if the logging system has been configured.
        if (not PropertyConfigurator.isConfigured()):

            print("WARNING : Logger is not configured")

        # Return the logger if it has already been configured.
        if (name in LoggerFactory.__loggers):
            return LoggerFactory.__loggers[name]

        # Find the logger level.
        loggerLogLevel = PropertyConfigurator.getLoggerLevel(name)

        # Get all root writers from properties (logger.rootLogger.writers) and instanciate them.
        rootWriters = PropertyConfigurator.getRootWriters()

        writers = {}

        for writerName in rootWriters:

            # Find the writer for this name.
            if 'logger.writer.{}'.format(writerName) not in PropertyConfigurator.getProperties().keys():
                print("ERROR : Not writer found for \"{}\"".format(writerName))
                return None

            # Retrieve the format and datetime format
            logFormat = PropertyConfigurator.getProperty('logger.writer.{}.format'.format(writerName), DEFAULT_LOG_FORMAT)
            datetimeFormat = PropertyConfigurator.getProperty('logger.writer.{}.dtformat'.format(writerName), DEFAULT_DATETIME_FORMAT)

            # Instanciate the writers.
            writerClassName = PropertyConfigurator.getProperty('logger.writer.{}'.format(writerName), 'Unknown')
            if writerClassName == 'ConsoleWriter':

                consoleWriter = ConsoleWriter()
                consoleWriter.setLogFormat(logFormat)
                consoleWriter.setDatetimeFormat(datetimeFormat)
                writers[writerName] = consoleWriter

            elif writerClassName == 'FileWriter':

                filename = PropertyConfigurator.getProperty('logger.writer.{}.filename'.format(writerName), None)
                if filename is None:
                    print("ERROR : Missing filename option for FileWriter")
                    return None

                append = PropertyConfigurator.getProperty('logger.writer.{}.append'.format(writerName), None)
                if append is None:
                    print("ERROR : Missing append option for FileWriter")
                    return None

                if append == 'false':
                    append = False
                elif append == 'true':
                    append = True
                else:
                    print("ERROR : Bad mode for append  option for FileWriter")
                    return None

                fileWriter = FileWriter(filename, append)
                fileWriter.setLogFormat(logFormat)
                fileWriter.setDatetimeFormat(datetimeFormat)
                writers[writerName] = fileWriter

            elif writerClassName == 'StringIOWriter':

                print("ERROR : StringIOWriter could not be instanciated from the logger factory")
                return None

            elif writerClassName == 'MultiWriter':

                print("ERROR : MultiWriter could not be instanciated from the logger factory")
                return None

            else:

                print("ERROR : Unknown writer \"{}\"".format(writerClassName))
                return None

        writer = MultiWriter(writers)
        logger = Logger(loggerLogLevel, name, writer)
        LoggerFactory.__loggers[name] = logger

        return logger

# ------------------------------------------------------------------------------
