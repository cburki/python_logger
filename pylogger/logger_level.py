# 
# Filename     : LoggerLevel.py
# Description  : Class defining the log level.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

class LoggerLevel():
    """
    Class representing the log levels.
    """

    # --------------------------------------

    EMERG = 7
    ALERT = 6
    CRIT = 5
    ERR = 4
    WARNING = 3
    NOTICE = 2
    INFO = 1
    DEBUG = 0

    DEFAULT_LEVEL = INFO

    string2LogLevel = {'EMERG': EMERG,
                       'ALERT': ALERT,
                       'CRIT': CRIT,
                       'ERR': ERR,
                       'WARNING': WARNING,
                       'NOTICE': NOTICE,
                       'INFO': INFO,
                       'DEBUG': DEBUG}

    logLevel2String = {EMERG: 'EMERGENCY',
                       ALERT: 'ALERT',
                       CRIT: 'CRITICAL',
                       ERR: 'ERROR',
                       WARNING: 'WARNING',
                       NOTICE: 'NOTICE',
                       INFO: 'INFO',
                       DEBUG: 'DEBUG'}

# ------------------------------------------------------------------------------
