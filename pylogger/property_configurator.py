# 
# Filename     : PropertyConfigurator.py
# Description  : Class allowing to configure the logger.
# Author       : Christophe Burki
#
######################################################################
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.
#
######################################################################

# ------------------------------------------------------------------------------

try:
    import ConfigParser
except:
    # python 3
    import configparser as ConfigParser

from pylogger.logger_level import LoggerLevel

# ------------------------------------------------------------------------------

class PropertyConfigurator():
    """
    Class to handle the logging properties.
    """

    # --------------------------------------

    __properties = {}
    DEFAULT_SECTION = "LOGGER"

    # --------------------------------------

    def __init__(self):
        """
        Constructor
        """

        pass

    # --------------------------------------

    @staticmethod
    def configureFromProperties(properties):
        """
        Configure the properties from a dictionary.

        :param properties: The properties to read
        :type properties: dictionary
        """

        PropertyConfigurator.__properties = properties

    # --------------------------------------

    @staticmethod
    def configureFromFile(filenames):
        """
        Configure the properties by reading them from the filename.
        The properties are inserted in a dictionary.

        :param filename: The file from which to read the properties
        :type filename: string
        """

        config = ConfigParser.RawConfigParser()
        config.optionxform = str
        config.read(filenames)
        for option, value in config.items(PropertyConfigurator.DEFAULT_SECTION):

            PropertyConfigurator.__properties[option] = value

    # --------------------------------------

    @staticmethod
    def clear():
        """
        Clear the properties.
        """

        PropertyConfigurator.__properties = {}

    # --------------------------------------

    @staticmethod
    def isConfigured():
        """
        Return if the properties are already configured or not.
        """

        return len(PropertyConfigurator.__properties) != 0

    # --------------------------------------

    @staticmethod
    def getProperties():
        """
        Return the configured properties.
        """

        return PropertyConfigurator.__properties

    # --------------------------------------

    @staticmethod
    def getProperty(key, defValue):
        """
        Return the property for the given key or the default value
        if the property with the given key does not exist.

        :param key: The key to get the property
        :type key: string
        :param defValue: The default value for non existant properties
        :type defValue: string
        """

        if key in PropertyConfigurator.__properties.keys():
            return PropertyConfigurator.__properties[key]

        return defValue

    # --------------------------------------

    @staticmethod
    def getRootWriters():
        """
        Return the root writers.
        """

        writers = []
        if 'logger.rootLogger.writers' not in PropertyConfigurator.__properties.keys():

            return writers

        values = PropertyConfigurator.__properties['logger.rootLogger.writers']
        writers = [v.strip() for v in values.split(',')]
        return writers

    # --------------------------------------

    @staticmethod
    def getRootLevel():
        """
        Return the level of the root writer.
        """

        if 'logger.rootLogger.level' in PropertyConfigurator.__properties.keys():

            return LoggerLevel.string2LogLevel[PropertyConfigurator.__properties['logger.rootLogger.level']]

        return LoggerLevel.DEFAULT_LEVEL

    # --------------------------------------

    @staticmethod
    def getLoggerLevel(name):
        """
        Return the configured level for the logger with the given name.

        :param name: The name of the logger
        :type name: string
        """

        if 'logger.level.{}'.format(name) in PropertyConfigurator.__properties.keys():

            return LoggerLevel.string2LogLevel[PropertyConfigurator.__properties['logger.level.{}'.format(name)]]

        return PropertyConfigurator.getRootLevel()

    # --------------------------------------

    @staticmethod
    def toString():
        """
        Return a string representation of the properties.
        """

        out = '{'
        i = 0
        keys = sorted(PropertyConfigurator.__properties.keys())

        for key in keys:
            out += "'{}': '{}'".format(key, PropertyConfigurator.__properties[key])
            if i < len(keys) - 1:
                out += ', '
            i += 1

        out += '}'
        return out

# ------------------------------------------------------------------------------
