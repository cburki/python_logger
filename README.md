[![build status](https://gitlab.com/cburki/python_logger/badges/master/build.svg)](https://gitlab.com/cburki/python_logger/commits/master)

---

Summary
-------

Configurable logging facility library for python. Output of messages are handled
by *writers* which defines where the messages are written. A message could be
written to several writers simultaneously each with their own output formats.
A logger can be created for each components of your system and defines different
levels for each components. Debug messages can also be easily enabled or disabled
for each components.

Here is a configuration properties sample with tow writers *CONSOLE* and *FILE*.
Log levels is declared for two components *Test_1* and *Test_2*.

    [LOGGER]
    logger.rootLogger.level      = INFO
    logger.rootLogger.writers    = CONSOLE, FILE
    logger.writer.CONSOLE        = ConsoleWriter
    logger.writer.CONSOLE.format = %d %l [%n] %f/%c %b : %m
    logger.writer.FILE           = FileWriter
    logger.writer.FILE.filename  = test_factory.log
    logger.writer.FILE.append    = false
    logger.writer.FILE.format    = %d %l [%n] : %m
    logger.writer.FILE.dtformat  = %Y-%m-%d
    logger.level.Test_1          = DEBUG
    logger.level.Test_2          = WARNING

The sample below show how to configure the logging system using the properties
file above and how to create a logger for the component *Test_2*.

    from property_configurator import PropertyConfigurator
    from logger_factory import LoggerFactory
    
    PropertyConfigurator.configureFromFile('logger.properties)
    myLogger = LoggerFactory.getLogger('Test_2')
    myLogger.error("This is an error message")
    myLogger.debug("This is a debug message")

The *debug* message is not written because for the *Test_2* logger the level
is set to *WARNING*.

More information about this library and how to use it can be found on the project web
site [https://python-logger.burkionline.net](https://python-logger.burkionline.net).


Dependency
----------

The only dependency is on [arrow](http://crsmithdev.com/arrow/) which is used to 
handle dates and times.


Installation
------------

The logging library and its dependency could simply be installed using pip.

    pip install arrow
    pip install git+https://gitlab.com/cburki/python_logger.git

Or, it could be installed from source.

    git clone https://gitlab.com/cburki/python_logger.git
    cd python_logger
    make install

The *make install* will take care of installing the dependency.
