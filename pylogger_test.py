# 
# Filename     : pylogger_test.py
# Description  : Testing code.
# Author       : Christophe Burki
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file LICENSE.  If not, write to the
# Free Software Foundation, Inc., 51 Franklin Street, Fifth
# ;; Floor, Boston, MA 02110-1301, USA.

# ------------------------------------------------------------------------------

from sys import exit
from io import StringIO
from unittest import TestCase, main

from pylogger.logger_factory import LoggerFactory
from pylogger.logger import Logger
from pylogger.logger_level import LoggerLevel
from pylogger.property_configurator import PropertyConfigurator
from pylogger.formatter import Formatter
from pylogger.location import Location
from pylogger.stringio_writer import StringIOWriter
from pylogger.console_writer import ConsoleWriter
from pylogger.file_writer import FileWriter
from pylogger.multi_writer import MultiWriter

import arrow

# ------------------------------------------------------------------------------

class Test(TestCase):
    """
    Unit test class.
    """

    # --------------------------------------

    def testOutput(self):
        """
        Write the log messages into a string io and check if what we have written
        is what we expect to be.
        """

        strIO = StringIO()
        writer = StringIOWriter(strIO)
        writer.setDatetimeFormat('%Y-%m-%d')
        logger = Logger(LoggerLevel.DEBUG, "TestOutput", writer)
        now = arrow.now()

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        want = '{} EMERGENCY [TestOutput] : This is an EMERGENCY message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.alert("This is an ALERT message")
        want = '{} ALERT [TestOutput] : This is an ALERT message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.crit("This is a CRITICAL message")
        want = '{} CRITICAL [TestOutput] : This is a CRITICAL message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.err("This is an ERROR message")
        want = '{} ERROR [TestOutput] : This is an ERROR message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.warning("This is a WARNING message")
        want = '{} WARNING [TestOutput] : This is a WARNING message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.notice("This is a NOTICE message")
        want = '{} NOTICE [TestOutput] : This is a NOTICE message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.info("This is an INFO message")
        want = '{} INFO [TestOutput] : This is an INFO message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        logger.debug("This is a DEBUG message")
        want = '{} DEBUG [TestOutput] : This is a DEBUG message'.format(now.format('YYYY-MM-DD'))
        got = strIO.getvalue()
        if want != got:
            strIO.close()
            self.fail('Error : want "{}", got "{}"'.format(want, got))
        strIO.truncate(0)
        strIO.seek(0)

        strIO.close()

    # --------------------------------------

    def testLevel(self):
        """
        Write the log message into a string io. Check the buffer length in order
        to see if the message was written or not. The string io for the INFO and
        DEBUG message must be of length 0 because the level is set to NOTICE.
        """

        strIO = StringIO()
        writer = StringIOWriter(strIO)
        logger = Logger(LoggerLevel.NOTICE, "TestLevel", writer)

        logger.emerg("This is an EMERGENCY message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : EMERGENCY message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.alert("This is an ALERT message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : ALERT message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.crit("This is a CRITICAL message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : CRITICAL message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.err("This is an ERROR message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : ERROR message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.warning("This is a WARNING message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : WARNING message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.notice("This is a NOTICE message")
        if len(strIO.getvalue()) == 0:
            strIO.close()
            self.fail("Error : NOTICE message must be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.info("This is an INFO message")
        if len(strIO.getvalue()) > 0:
            strIO.close()
            self.fail("Error : INFO message must not be written")
        strIO.truncate(0)
        strIO.seek(0)

        logger.debug("This is a DEBUG message")
        if len(strIO.getvalue()) > 0:
            strIO.close()
            self.fail("Error : DEBUG message must not be written")
        strIO.truncate(0)
        strIO.seek(0)

        strIO.close()

    # --------------------------------------

    def testStringIOWriter(self):
        """
        Test writing message to a string io.
        INFO adn DEBUG message must be empty.
        """

        strIO = StringIO()
        writer = StringIOWriter(strIO)
        logger = Logger(LoggerLevel.NOTICE, "TestStringIOWriter", writer)

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.alert("This is an ALERT message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.crit("This is a CRITICAL message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.err("This is an ERROR message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.warning("This is a WARNING message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.notice("This is a NOTICE message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.info("This is an INFO message")
        print("string={}".format(strIO.getvalue()))

        strIO.truncate(0)
        logger.debug("This is a DEBUG message")
        print("string={}".format(strIO.getvalue()))

        strIO.close()

    # --------------------------------------

    def testConsoleWriter(self):
        """
        Test writing log message to the console.
        INFO and DEBUG messages must not be shown.
        """

        writer = ConsoleWriter()
        writer.setLogFormat('%d %l [%n] %f/%c %b : %m')
        logger = Logger(LoggerLevel.NOTICE, "TestConsoleWriter", writer)

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        logger.alert("This is an ALERT message")
        logger.crit("This is a CRITICAL message")
        logger.err("This is an ERROR message")
        logger.warning("This is a WARNING message")
        logger.notice("This is a NOTICE message")
        logger.info("This is an INFO message")
        logger.debug("This is a DEBUG message")

    # --------------------------------------

    def testFileWriter(self):
        """
        Test writing message ing a file.
        Compare the lines written and what we expect to be written.
        """

        filename = "test.log"
        writer = FileWriter(filename, False)
        writer.setLogFormat('%l [%n] : %m')
        writer.setDatetimeFormat('%Y-%m-%d')
        logger = Logger(LoggerLevel.DEBUG, "TestFileWriter", writer)

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        logger.alert("This is an ALERT message")
        logger.crit("This is a CRITICAL message")
        logger.err("This is an ERROR message")
        logger.warning("This is a WARNING message")
        logger.notice("This is a NOTICE message")
        logger.info("This is an INFO message")
        logger.debug("This is a DEBUG message")

        wants = ["EMERGENCY [TestFileWriter] : This is an EMERGENCY message\n",
                 "ALERT [TestFileWriter] : This is an ALERT message\n",
                 "CRITICAL [TestFileWriter] : This is a CRITICAL message\n",
                 "ERROR [TestFileWriter] : This is an ERROR message\n",
                 "WARNING [TestFileWriter] : This is a WARNING message\n",
                 "NOTICE [TestFileWriter] : This is a NOTICE message\n",
                 "INFO [TestFileWriter] : This is an INFO message\n",
                 "DEBUG [TestFileWriter] : This is a DEBUG message\n"]

        logFile = open(filename, 'r')
        lines = logFile.readlines()
        logFile.close()
        i = 0

        for line in lines:

            self.assertEqual(line, wants[i], 'Error : want "{}", got "{}"'.format(wants[i], line))
            i += 1

    # --------------------------------------

    def testMultiWriter(self):
        """
        Test writing log message to both a file a standatd output.
        INFO and DEBUG message must be shown.
        """

        filename = "test_multi.log"
        cWriter = ConsoleWriter()
        fWriter = FileWriter(filename, False)
        writers = {'CONSOLE': cWriter, 'FILE': fWriter}
        writer = MultiWriter(writers)
        logger = Logger(LoggerLevel.NOTICE, "TestMultiWriter", writer)

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        logger.alert("This is an ALERT message")
        logger.crit("This is a CRITICAL message")
        logger.err("This is an ERROR message")
        logger.warning("This is a WARNING message")
        logger.notice("This is a NOTICE message")
        logger.info("This is an INFO message")
        logger.debug("This is a DEBUG message")

    # --------------------------------------

    def testFormatter(self):
        """
        Test the formatter.
        """

        location = Location('Filename', 'Funcname', 0)

        # Test message formatYYYY-MM-DDYYYY-MM-DD
        formatter = Formatter('%l [%n] %f/%c %b : %m', '%Y-%m-%d %H:%M:%S')
        message = formatter.formatLog("MESSAGE", "NAME", LoggerLevel.INFO, arrow.now(), location)
        want = "INFO [NAME] Filename/Funcname 0 : MESSAGE"
        self.assertEqual(message, want, 'Error : want "{}", got "{}"'.format(want, message))

        # Test datetime format
        refTime = arrow.get('2016-03-28 17:32:53', 'YYYY-MM-DD HH:mm:ss')
        want = '2016-03-28 17:32:53'
        datetime = formatter.formatDatetime(refTime)
        self.assertEqual(datetime, want, 'Error : want "{}", got "{}"'.format(want, datetime))

    # --------------------------------------

    def testPropertyConfigurator(self):
        """
        Test property configurator.
        """

        # Test missing required properties.
        propMissing = {'logger.writer.CONSOLE': 'ConsoleWriter'}
        PropertyConfigurator.configureFromProperties(propMissing)

        want = "{'logger.level.Test_1': 'DEBUG', 'logger.level.Test_2': 'WARNING', 'logger.rootLogger.level': 'INFO', 'logger.rootLogger.writers': 'CONSOLE, FILE', 'logger.writer.CONSOLE': 'ConsoleWriter', 'logger.writer.CONSOLE.format': '%d %l [%n] %f/%c %b : %m', 'logger.writer.FILE': 'FileWriter', 'logger.writer.FILE.append': 'false', 'logger.writer.FILE.dtformat': '%Y-%m-%d', 'logger.writer.FILE.filename': 'test_factory.log', 'logger.writer.FILE.format': '%d %l [%n] : %m'}"

        # Test property configuration from dict.
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'CONSOLE, FILE',
                 'logger.writer.CONSOLE': 'ConsoleWriter',
                 'logger.writer.CONSOLE.format': '%d %l [%n] %f/%c %b : %m',
                 'logger.writer.FILE': 'FileWriter',
                 'logger.writer.FILE.filename': 'test_factory.log',
                 'logger.writer.FILE.append': 'false',
                 'logger.writer.FILE.format': '%d %l [%n] : %m',
                 'logger.writer.FILE.dtformat': '%Y-%m-%d',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        got = PropertyConfigurator.toString()
        self.assertEqual(want, got, 'Error : want "{}", got "{}"'.format(want, got))

        # Test property configuration from file (single file).
        PropertyConfigurator.clear()
        PropertyConfigurator.configureFromFile('test.properties')
        got = PropertyConfigurator.toString()
        self.assertEqual(want, got, 'Error : want "{}", got "{}"'.format(want, got))

        # Test property configuration from file (array of files).
        PropertyConfigurator.clear()
        PropertyConfigurator.configureFromFile(['test.properties'])
        got = PropertyConfigurator.toString()
        self.assertEqual(want, got, 'Error : want "{}", got "{}"'.format(want, got))

    # --------------------------------------

    def testLoggerFactory(self):
        """
        Test the property configurator.
        """

        # Test no writers defined
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'CONSOLE',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        logger = LoggerFactory.getLogger("TestNoWriter")
        self.assertIsNone(logger, "Error : must failed when no writer configured")

        # Test unknown writer
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'CONSOLE',
                 'logger.writer.CONSOLE': 'UnknownWriter',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        logger = LoggerFactory.getLogger("TestUnknownWriter")
        self.assertIsNone(logger, "Error : must failed when unknown writer configured")

        # Test bad properties for file writer
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'FILE',
                 'logger.writer.FILE': 'FileWriter',
                 'logger.writer.FILE.append': 'false',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        logger = LoggerFactory.getLogger("TestBadFile")
        self.assertIsNone(logger, "Error : must failed when missing file writer option")

        # Test using string io writer
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'STRINGIO',
                 'logger.writer.STRINGIO': 'StringIOWriter',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        logger = LoggerFactory.getLogger("TestStringIOWriter")
        self.assertIsNone(logger, "Error : must failed when using string io writer")

        # Test using multi writer
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'MULTI',
                 'logger.writer.MULTI': 'MultiWriter',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)
        logger = LoggerFactory.getLogger("TestMultiWriter")
        self.assertIsNone(logger, "Error : must failed when using multi writer")

        # Test logging
        props = {'logger.rootLogger.level': 'INFO',
                 'logger.rootLogger.writers': 'CONSOLE, FILE',
                 'logger.writer.CONSOLE': 'ConsoleWriter',
                 'logger.writer.CONSOLE.format': '%d %l [%n] %f/%c %b : %m',
                 'logger.writer.FILE': 'FileWriter',
                 'logger.writer.FILE.filename': 'test_factory.log',
                 'logger.writer.FILE.append': 'false',
                 'logger.writer.FILE.format': '%d %l [%n] : %m',
                 'logger.writer.FILE.dtformat': '%m-%d-%m-%d',
                 'logger.level.Test_1': 'DEBUG',
                 'logger.level.Test_2': 'WARNING'}
        PropertyConfigurator.configureFromProperties(props)

        logger1 = LoggerFactory.getLogger("Test_1")
        self.assertIsNotNone(logger1, "Error : unable to get logger1")

        logger2 = LoggerFactory.getLogger("Test_2")
        self.assertIsNotNone(logger2, "Error : unable to get logger2")

        s = "EMERGENCY"
        logger1.emerg("This is an {} message".format(s))
        logger1.alert("This is an ALERT message")
        logger1.crit("This is a CRITICAL message")
        logger1.err("This is an ERROR message")
        logger1.warning("This is a WARNING message")
        logger1.notice("This is a NOTICE message")
        logger1.info("This is an INFO message")
        logger1.debug("This is a DEBUG message")

        logger2.emerg("This is an {} message".format(s))
        logger2.alert("This is an ALERT message")
        logger2.crit("This is a CRITICAL message")
        logger2.err("This is an ERROR message")
        logger2.warning("This is a WARNING message")
        logger2.notice("This is a NOTICE message")
        logger2.info("This is an INFO message")
        logger2.debug("This is a DEBUG message")

        logger1b = LoggerFactory.getLogger("Test_1")
        self.assertEqual(logger1, logger1b, "Error : this is not the same logger")

    # --------------------------------------

    def testSetupFromCode(self):
        """
        Test the configuration of the logging system using code.
        """

        PropertyConfigurator.clear()

        logger = LoggerFactory.getLogger('Test')
        consoleWriter = ConsoleWriter()
        fileWriter = FileWriter('test_setup.log', True)
        logger.addWriter('CONSOLE', consoleWriter)
        logger.addWriter('FILE', fileWriter)
        logger.setLevel(LoggerLevel.NOTICE)
        logger.getWriter('CONSOLE').setLogFormat('%d %l [%n] %f/%c (%b) : %m')

        s = "EMERGENCY"
        logger.emerg("This is an {} message".format(s))
        logger.alert("This is an ALERT message")
        logger.crit("This is a CRITICAL message")
        logger.err("This is an ERROR message")
        logger.warning("This is a WARNING message")
        logger.notice("This is a NOTICE message")
        logger.info("This is an INFO message")
        logger.debug("This is a DEBUG message")

        # Log messages will no longer been written to the console
        logger.removeWriter('CONSOLE')
        logger.emerg("This is an {} message".format(s))
        logger.alert("This is an ALERT message")
        logger.crit("This is a CRITICAL message")
        logger.err("This is an ERROR message")
        logger.warning("This is a WARNING message")
        logger.notice("This is a NOTICE message")
        logger.info("This is an INFO message")
        logger.debug("This is a DEBUG message")

# ------------------------------------------------------------------------------

if (__name__ == "__main__"):

    main()
    print("OK")
    exit(0)

# ------------------------------------------------------------------------------
